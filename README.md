# etcpd - Extendable TCP/IP Daemon
etcpd is a TCP/IP daemon which is written in C++.

## OS Support
- [x] NetBSD
- [x] Other BSDs (Should work. I haven't checked yet.)
- [x] OSX (Also should work. I haven't checked yet.)
- [x] Linux (Checked working on Void 6.x)
- [ ] Windows (I maybe will support in the future... But I don't have any Windows machine. )

## Features
- Module loading
- Fast
- Under 1MB

## Default modules
- HTTP/1.1 Module
  - File fetching
  - Indexing

## Building etcpd
### Needed Libraries/Tools
- OpenSSL (required only if `--enable-ssl` is attached to the `./configure`)
- GNU make (`gmake` in pkgsrc)

You can attach `--enable-pthread` to `./configure` to make it use pthread for multithreading.

```shell-session
$ ./configure --prefix=/usr
$ gmake all
# gmake install
```
