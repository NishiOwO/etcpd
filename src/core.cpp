#include "../include/macro.h"

#include "../include/util.h"
#include "../include/info.h"

#include <unistd.h>
#include <sys/utsname.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <dlfcn.h>

#ifdef USE_SSL
#include <openssl/ssl.h>
#endif

#include <time.h>

#ifdef USE_PTHREAD
#warning "Using pthread for multithreading"

#include <pthread.h>
#else
#warning "Using fork() for multithreading"
#endif

#ifdef USE_SSL
#warning "Compiling with the SSL support"
#endif

#include <signal.h>

//#define TESTING

extern "C" ssize_t gwrite(etcpd::module_config config, const void* buf, size_t size){
#ifdef USE_SSL
	if(config.ssl == NULL){
#endif
		// If config.ssl is NULL - it's not SSL
		return write(config.client_socket, buf, size);
#ifdef USE_SSL
	}else{
		return SSL_write(config.ssl, buf, size);
	}
#endif
}

extern "C" ssize_t gread(etcpd::module_config config, void* buf, size_t size){
#ifdef USE_SSL
	if(config.ssl == NULL){
#endif
		// If config.ssl is NULL - it's not SSL
		return read(config.client_socket, buf, size);
#ifdef USE_SSL
	}else{
		return SSL_read(config.ssl, buf, size);
	}
#endif
}

extern "C" int gclose(etcpd::module_config config){
#ifdef USE_SSL
	if(config.ssl == NULL){
#endif
		// If config.ssl is NULL - it's not SSL
		return close(config.client_socket);
#ifdef USE_SSL
	}else{
		int result = SSL_shutdown(config.ssl);
		SSL_free(config.ssl);
		return result;
	}
#endif
}

extern "C" std::string gversion(){
	return VERSION_FULL;
}

extern "C" std::string gplatform(){
	return PLATFORM_FULL;
}

extern "C" std::string grunning(){
	struct utsname uname_struct;
	uname(&uname_struct);
	std::string result = "";
	result += uname_struct.sysname;
	result += "/";
	result += uname_struct.machine;
	result += " ";
	result += uname_struct.release;
	return result;
}

struct arg_struct {
	char* string;
	int client_socket;
	etcpd::module_config config;
};

void* thread_function(void* arguments){
	struct arg_struct* args = (struct arg_struct*)arguments;
	char* s = args->string;
	int client_socket = args->client_socket;
	etcpd::module_config config = args->config;
	bool verbose = config.verbose;
#ifdef USE_SSL
	SSL_CTX* ctx;
	SSL* ssl = NULL;
	
	if(config.parsed.ssl.size() != 0){
		DEBUG_NONL("SSL will be used: ");
		std::string ident = config.parsed.ssl.substr(0, config.parsed.ssl.find_first_of("v"));
		std::string version = config.parsed.ssl.substr(config.parsed.ssl.find_first_of("v") + 1);
		DEBUG_CONT(ident + " version " + version);
		DEBUG("Initialising OpenSSL");
		SSL_library_init();
		if(version == ident && version == "TLS"){
			DEBUG("Will use TLS_server_method()");
			ctx = SSL_CTX_new(TLS_server_method());
		}else{
			if(ident == "SSL"){
				if(version == "23"){
					DEBUG("Will use SSLv23_server_method()");
					ctx = SSL_CTX_new(SSLv23_server_method());
				}else{
					DEBUG("Unknown SSL version, exiting");
					std::cerr << "SSL version " << version << "is unknown, exiting" << std::endl;
					exit(1);
				}
			}else if(ident == "TLS"){
				if(version == "1"){
					DEBUG("Will use TLSv1_server_method()");
					ctx = SSL_CTX_new(TLSv1_server_method());
				}else if(version == "1_1"){
					DEBUG("Will use TLSv1_1_server_method()");
					ctx = SSL_CTX_new(TLSv1_1_server_method());
				}else if(version == "1_2"){
					DEBUG("Will use TLSv1_2_server_method()");
					ctx = SSL_CTX_new(TLSv1_2_server_method());
				}else{
					DEBUG("Unknown TLS version, exiting");
					std::cerr << "TLS version " << version << "is unknown, exiting" << std::endl;
					exit(1);
				}
			}
		}
		SSL_CTX_use_certificate_file(ctx, config.parsed.ssl_certificate.c_str(), SSL_FILETYPE_PEM);
		SSL_CTX_use_PrivateKey_file(ctx, config.parsed.ssl_key.c_str(), SSL_FILETYPE_PEM);
		ssl = SSL_new(ctx);
		SSL_set_fd(ssl, client_socket);
		SSL_accept(ssl);
	}
#endif
	for(etcpd::server_module loaded_module : config.parsed.modules){
		etcpd::module_config conf;
		conf = config;
		conf.ip = std::string(s);
		conf.client_socket = client_socket;
#ifdef USE_SSL
		conf.ssl = ssl;
#endif
		etcpd::module_launcher func = (etcpd::module_launcher)loaded_module.run;
		int result = func(conf);
		parse_result(result, conf, loaded_module.path);
	}
	//config.ip = std::string(s);
	//config.client_socket = client_socket;
	//config.ssl = ssl;
	//gclose(config);
	return NULL;
}

std::string pid_file;
void delete_pid_file(int sig){
	remove(pid_file.c_str());
	exit(0);
}

extern "C" void init(etcpd::module_config config){
	bool verbose = config.verbose;
	DEBUG("");
	DEBUG("4. Core");
	struct sockaddr_in addr;
	int server_socket = socket(AF_INET, SOCK_STREAM, 0);
	addr.sin_family = AF_INET;
	addr.sin_port = htons(config.parsed.port);
	addr.sin_addr.s_addr = inet_addr("0.0.0.0");
	
	if(server_socket < 0){
		DEBUG("socket() failed");
		std::cerr << "Server socket creation failed" << std::endl;
		exit(-1);
	}
	
	int ret = bind(server_socket, (struct sockaddr*)&addr, sizeof(addr));

	if(ret < 0){
		DEBUG("bind() failed");
		std::cerr << "Binding the server socket failed" << std::endl;
		exit(-1);
	}

	ret = listen(server_socket, config.parsed.max_clients);
	
	if(ret < 0){
		DEBUG("listen() failed");
		std::cerr << "Listening to the socket failed" << std::endl;
		exit(-1);
	}

	DEBUG("Initialising modules");
	for(etcpd::server_module loaded_module : config.parsed.modules){
		etcpd::module_config conf;
		conf = config;
		etcpd::module_launcher func = (etcpd::module_launcher)loaded_module.init;
		func(conf);
	}
	
	std::cout << "etcpd: Started at " << etcpd::date() << std::endl;
	int pid;
	if(config.daemonize){
		DEBUG("Server ready, forking");
		pid = fork();
	}else{
		DEBUG("Server ready");
		pid = 0;
	}

	if(pid == 0){
		pid_file = "/tmp/etcpd";
		int counter;
		for(counter = 0; !!std::ifstream(pid_file + std::to_string(counter) + ".pid"); counter++);
		pid_file += std::to_string(counter);
		pid_file += ".pid";
		std::ofstream ofs(pid_file);
		ofs << std::to_string(getpid());
		ofs.close();
		DEBUG("PID file is " + pid_file);
		signal(SIGKILL, delete_pid_file);
		signal(SIGTERM, delete_pid_file);
		signal(SIGINT, delete_pid_file);
		while(true){
			struct sockaddr_in client_address;
			socklen_t address_length = sizeof(client_address);
			int client_socket = accept(server_socket, (struct sockaddr*)&client_address, &address_length);
			if(client_socket == -1){
				continue;
			}
			char s[INET6_ADDRSTRLEN > INET_ADDRSTRLEN ? INET6_ADDRSTRLEN : INET_ADDRSTRLEN];
			switch(client_address.sin_family){
				case AF_INET: {
					inet_ntop(AF_INET, &(client_address.sin_addr), s, INET_ADDRSTRLEN);
					break;
				}
				case AF_INET6: {
					struct sockaddr_in6* addr_in6 = (struct sockaddr_in6*)&client_address;
					inet_ntop(AF_INET6, &(addr_in6->sin6_addr), s, INET6_ADDRSTRLEN);
					break;
				}
				default:
					break;
			}
			DEBUG("Connection from " + std::string(s));
			
			signal(SIGPIPE, SIG_IGN);
			signal(SIGCHLD, SIG_IGN);
			
			struct arg_struct args;
			args.config = config;
			args.string = (char*)s;
			args.client_socket = client_socket;

#ifdef USE_PTHREAD
			pthread_t thr;
			pthread_create(&thr, NULL, &thread_function, (void*)&args);

			sigset_t oldset, newset;
			sigemptyset(&newset);
			sigaddset(&newset, SIGPIPE);
			pthread_sigmask(SIG_BLOCK, &newset, &oldset);
			pthread_detach(thr);
#else
			if(fork() == 0){
				thread_function(&args);
				_exit(0);
			}
#endif
		}
		delete_pid_file(-1);
		exit(-1);
	}else{
		DEBUG("Parent process: exiting");
		exit(0);
	}
}
