#include "../include/mo_http.h"
#include "../include/util.h"
#include "../include/macro.h"

#include <sys/stat.h>
#include <dlfcn.h>
#include <dirent.h>
#include <fstream>

std::vector<std::string> deny_path;

extern "C" void init(etcpd::module_config config){
	bool verbose = config.verbose;
	DEBUG("mo_http_basic.so v1.1.0");
	std::string path = "";
	for(char c : config.parsed.custom_data["HTTP-DENY"]){
		if(c == ','){
			if(path[path.size() - 1] == '/') path = path.substr(0, path.size() - 1);
			std::cout << path;
			deny_path.push_back(path);
			path = "";
		}else{
			path += c;
		}
	}
	if(path.size() != 0){
		if(path[path.size() - 1] == '/') path = path.substr(0, path.size() - 1);
		deny_path.push_back(path);
	}
}

std::string _config_error(etcpd::module_config config, std::string msg){
	gload(config);
	std::string result = "";
	result += "<html>\n";
	result += "	<head>\n";
	result += "		<meta charset='UTF-8'>\n";
	result += "		<title>!!! Config Error !!!</title>\n";
	result += "	</head>\n";
	result += "	<body>\n";
	result += "		<h1 style='color: red;'>Config Error</h1>\n";
	result += "		<hr>\n";
	result += "		Sorry, some module caused the error:\n";
	result += "<pre>";
	result += msg;
	result += "</pre>\n<br>";
	result += "		<hr>\n";
	result += "		<i>etcpd-core/" + gversion() + " was built on " + gplatform() + ", running on " + grunning() + "</i>\n";
	result += "	</body>\n";
	result += "</html>";
	return result;
}

extern "C" int run(etcpd::module_config config, mo_http::http_request request, mo_http::http_response response){
	int bytes_per_write = 512;
	if(config.parsed.custom_data.find("HTTP-BYTES-PER-WRITE") != config.parsed.custom_data.end()){
		bytes_per_write = stoi(config.parsed.custom_data["HTTP-BYTES-PER-WRITE"]);
	}
	bool verbose = config.verbose;
	DEBUG("[" + std::to_string(config.client_socket) + "] run@mo_http_basic.so");
	gload(config);
	if(!etcpd::find_value<std::string, std::string>(config.parsed.custom_data, "HTTP-ROOT")){
		response.status_code = 500;
		response.status_text = "Internal Server Error";
		std::string error = _config_error(config, "mo_http_basic.so: HTTP-ROOT is not set");
		response.headers.push_back("Content-Length: " + std::to_string(error.size()));
		response.headers.push_back("Content-Type: text/html");
		response.http_header();
		gwrite(config, error.c_str(), error.size());
		return etcpd::MODULE_BREAK;
	}else{
		chdir(config.parsed.custom_data["HTTP-ROOT"].c_str());
		std::string path = "./" + request.path;
		struct stat s;
		bool deny = false;
		for(std::string match_path : deny_path){
			if(match_path == request.path){
				deny = true;
				break;
			}
		}
		if(deny){
			response.status_code = 403;
			response.status_text = "Forbidden";
			std::ifstream ifs;
			std::string path_http_result;
			HTTP_FILE_CHECK(403);
			std::string mime;
			GET_MIME(path_http_result);
			response.headers.push_back("Content-Type: " + mime);
			response.headers.push_back("Content-Length: " + std::to_string(s.st_size));
			response.http_header();
			std::string body = "";
			char c;
			off_t count = 0;
			while(ifs >> std::noskipws >> c){
				count++;
				body += c;
				if(count == bytes_per_write){
					gwrite(config, body.c_str(), count);
					body = "";
					count = 0;
				}
			}
			if(count != 0){
				gwrite(config, body.c_str(), count);
			}
		}
		if(stat(path.c_str(), &s) != 0){
			response.status_code = 404;
			response.status_text = "Not Found";
			std::ifstream ifs;
			std::string path_http_result;
			HTTP_FILE_CHECK(404);
			std::string mime;
			GET_MIME(path_http_result);
			response.headers.push_back("Content-Type: " + mime);
			response.headers.push_back("Content-Length: " + std::to_string(s.st_size));
			response.http_header();
			std::string body = "";
			char c;
			off_t count = 0;
			while(ifs >> std::noskipws >> c){
				count++;
				body += c;
				if(count == bytes_per_write){
					gwrite(config, body.c_str(), count);
					body = "";
					count = 0;
				}
			}
			if(count != 0){
				gwrite(config, body.c_str(), count);
			}
		}else{
			bool directory = false;
			if(S_ISDIR(s.st_mode)){
				chdir(config.parsed.custom_data["HTTP-ROOT"].c_str());
				std::string path_ = "./" + request.path;
				DIR* d;
				d = opendir(path_.c_str());
				struct dirent* dir;
				if(d == NULL){
					gclose(config);
					return etcpd::MODULE_BREAK;
				}
				while((dir = readdir(d)) != NULL){
					try{
						if(std::string(dir->d_name) == "index.html" || std::string(dir->d_name) == "index.htm"){
							path += "/";
							path += dir->d_name;
							stat(path.c_str(), &s);
							break;
						}
					}catch(...){}
				}
			}
			if(!S_ISDIR(s.st_mode)){
				std::string mime;
				std::ifstream ifs(path);
				GET_MIME(path);
				response.headers.push_back("Content-Type: " + mime);
				response.headers.push_back("Content-Length: " + std::to_string(s.st_size));
				response.http_header();
				std::string body = "";
				char c;
				off_t count = 0;
				while(ifs >> std::noskipws >> c){
					count++;
					body += c;
					if(count == bytes_per_write){
						gwrite(config, body.c_str(), count);
						body = "";
						count = 0;
					}
				}
				if(count != 0){
					gwrite(config, body.c_str(), count);
				}
			}else{
				if(config.parsed.custom_data.find("400-ON-DIRECTORY") != config.parsed.custom_data.end()){
					response.status_code = 400;
					response.status_text = "Bad Request";
					std::ifstream ifs;
					std::string path_http_result;
					HTTP_FILE_CHECK(400);
					std::string mime;
					GET_MIME(path_http_result);
					response.headers.push_back("Content-Type: " + mime);
					response.headers.push_back("Content-Length: " + std::to_string(s.st_size));
					response.http_header();
					std::string body = "";
					char c;
					off_t count = 0;
					while(ifs >> std::noskipws >> c){
						count++;
						body += c;
						if(count == bytes_per_write){
							gwrite(config, body.c_str(), count);
							body = "";
							count = 0;
						}
					}
					if(count != 0){
						gwrite(config, body.c_str(), count);
					}
				}else{
					return etcpd::MODULE_CONTINUE;
				}
			}
		}
		return etcpd::MODULE_BREAK;
	}
	return etcpd::MODULE_CONTINUE;
}
