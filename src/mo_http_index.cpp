#include "../include/mo_http.h"
#include "../include/util.h"
#include "../include/macro.h"

#include <sys/stat.h>
#include <dlfcn.h>
#include <dirent.h>

std::vector<std::string> hide_path;

extern "C" void init(etcpd::module_config config){
	bool verbose = config.verbose;
	DEBUG("mo_http_index.so v1.1.0");
	std::string path = "";
	for(char c : config.parsed.custom_data["HTTP-INDEX-HIDE"]){
		if(c == ','){
			if(path[path.size() - 1] == '/') path = path.substr(0, path.size() - 1);
			std::cout << path;
			hide_path.push_back(path);
			path = "";
		}else{
			path += c;
		}
	}
	if(path.size() != 0){
		if(path[path.size() - 1] == '/') path = path.substr(0, path.size() - 1);
		hide_path.push_back(path);
	}
}

extern "C" int run(etcpd::module_config config, mo_http::http_request request, mo_http::http_response response){
	bool verbose = config.verbose;
	DEBUG("[" + std::to_string(config.client_socket) + "] run@mo_http_index.so");
	gload(config);
	std::string result = "";
	result += "<html>\n";
	result += "	<head>\n";
	result += "		<meta charset='utf-8'>\n";
	result += "		<title>Index Of " + request.path + "</title>\n";
	result += "	</head>\n";
	result += "	<body>\n";
	result += "		<h1>Index Of " + request.path + "</h1>\n";
	result += "		<hr>\n";
	result += "		<table border='0' style='width:100%;'>\n";
	result += "			<tr style='background:black;color:white;'>\n";
	result += "				<th>\n";
	result += "					Name\n";
	result += "				</th>\n";
	result += "				<th style='width:20%;'>\n";
	result += "					Type\n";
	result += "				</th>\n";
	result += "				<th style='width:20%;'>\n";
	result += "					Size\n";
	result += "				</th>\n";
	result += "			</tr>\n";
	
	DIR* d;
	struct dirent* dir;

	chdir(config.parsed.custom_data["HTTP-ROOT"].c_str());
	d = opendir(("./" + request.path).c_str());

	if(d == NULL){
		gclose(config);
		return etcpd::MODULE_BREAK;
	}

	while((dir = readdir(d)) != NULL){
		std::string path = dir->d_name;
		std::string real_path = path;
		std::string real_path2 = path;
		std::string type = "";
		std::string size = "";
		struct stat s;
		if(path == ".") continue;
		if(path == "..") path = "Parent directory";
		real_path = request.path + (request.path[request.path.size() - 1] == '/' ? "" : "/") + real_path;
		stat(("./" + real_path).c_str(), &s);
		if(S_ISDIR(s.st_mode)){
			type = "Directory";
		}else{
			std::string mime;
			GET_MIME(real_path);
			type = mime;
			std::string byte_prefix = "";
			long double final_size = s.st_size;
			if(s.st_size < 1024){
			}else if(s.st_size / 1024 < 1024){
				final_size /= 1024;
				byte_prefix = "K";
			}else if(s.st_size / 1024 / 1024 < 1024){
				final_size /= 1024 * 1024;
				byte_prefix = "M";
			}else if(s.st_size / 1024 / 1024 / 1024 < 1024){
				final_size /= 1024 * 1024 * 1024;
				byte_prefix = "G";
			}else if(s.st_size / 1024 / 1024 / 1024 / 1024 < 1024){
				final_size /= 1024 * 1024 * 1024;
			       	final_size /= 1024;
				byte_prefix = "T";
			}

			size = std::to_string(final_size);
			size = size.substr(0, size.find_first_of(".") + 2);
			size += " " + byte_prefix + "B";
		}

		std::string buffer_path = "";
		for(int i = 0; i < real_path.size(); i++){
			if(real_path[i] == '\''){
				buffer_path += "%27";
			}else if(real_path[i] == '<'){
				buffer_path += "%3C";
			}else if(real_path[i] == '>'){
				buffer_path += "%3E";
			}else{
				buffer_path += real_path[i];
			}
		}
		real_path = buffer_path;
		bool hide = false;
		for(std::string match_path : hide_path){
			if(match_path == real_path){
				hide = true;
				break;
			}
		}
		if(hide) continue;

		result += "			<tr>\n";
		result += "				<td>\n";
		result += "					<a href='" + real_path + "/'>" + path + "</a>";
		result += "				</td>\n";
		result += "				<td>\n";
		result += "					" + type + "\n";
		result += "				</td>\n";
		result += "				<td>\n";
		result += "					" + size + "\n";
		result += "				</td>\n";
		result += "			</tr>\n";
	}

	result += "		</table>\n";
	result += "		<hr>\n";
	result += "		<i>etcpd-core/" + gversion() + " was built on " + gplatform() + ", running on " + grunning() + "</i>\n";
	result += "		<style>\n";
	result += "			html {\n";
	result += "				font-family: Monospace;\n";
	result += "			}\n";
	result += "		</style>\n";
	result += "	</body>\n";
	result += "</html>";
	response.headers.push_back("Content-Type: text/html");
	response.headers.push_back("Content-Length: " + std::to_string(result.size()));
	response.http_header();
	gwrite(config, result.c_str(), result.size());
	return etcpd::MODULE_BREAK;
}
