#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <cstring>
#include <unistd.h>

#include "../include/info.h"
#include "../include/macro.h"
#include "../include/util.h"

#define COPYRIGHT_YEAR std::string("2022")

std::string process(std::string text){
	std::string result = "";
	for(int i = 0; i < text.size(); i++){
		if(text[i] == '#'){
			result += "\x1b[47m \x1b[m";
		}else{
			result += text[i];
		}
	}
	return result;
}

int main(int argc, char** argv){
	std::vector<std::string> args(argv, argv + argc);
	bool verbose = false;
	bool first_arg = false;
	std::string config_file = "/etc/etcpd.conf";
	bool ignore_next = false;
	bool daemonize = true;
	int counter = -1;
	for(std::string arg : args){
		counter++;
		if(!first_arg){
			first_arg = true;
			continue;
		}
		if(ignore_next){
			ignore_next = false;
			continue;
		}
		if(arg == "-v" || arg == "--verbose"){
			verbose = true;
		}else if(arg == "-c" || arg == "--config"){
			config_file = args[counter + 1];
			ignore_next = true;
		}else if(arg == "-V" || arg == "--version"){
			std::cout << "etcpd version " << VERSION_FULL << std::endl;
			std::cout << "Copyright (C) 2022 EEE++ tEam." << std::endl;
			std::cout << "etcpd is licensed under the 3-clause BSD license" << std::endl;
			return 0;
		}else if(arg == "-D"){
			daemonize = false;
		}else if(arg == "-d"){
			daemonize = true;
		}else if(arg == "-h" || arg == "--help"){
			std::cout << "Usage: " << args[0] << " [options]" << std::endl;
			std::cout << "  --version" << std::endl;
			std::cout << "  -V               Display the version of the etcpd." << std::endl;
			std::cout << "  --help" << std::endl;
			std::cout << "  -h               Display this information." << std::endl;
			std::cout << "  --verbose" << std::endl;
			std::cout << "  -v               Run etcpd with the verbose mode." << std::endl;
			std::cout << "  --config <file>" << std::endl;
			std::cout << "  -c <file>        Set the config path. (Default is /etc/etcpd.conf)" << std::endl;
			std::cout << "  -d               Daemonize the etcpd. (Default)" << std::endl;
			std::cout << "  -D               Do not daemonize the etcpd." << std::endl;
			return 0;
		}else{
			std::cerr << "Unknown option: " << arg << std::endl;
			return 1;
		}
	}
	DEBUG(process(" #####            #####  ###  ####       ####       "));
	DEBUG(process(" #                  #   #   # #   #      #   #      "));
	DEBUG(process(" #####              #   #     ####       #   #      "));
	DEBUG(process(" #                  #   #   # #          #   #      "));
	DEBUG(process(" ##### xtendable    #    ###  # /IP      #### aemon "));
	DEBUG("Fast, Simple, Extendable - Written in C++");
	DEBUG("Copyright (C) " + COPYRIGHT_YEAR + " EEE++ tEam.");
	DEBUG("== V   E   R   B   O   S   E       M   O   D   E ==");
	DEBUG("---------------------------------------------------");
	DEBUG("Starting etcpd version " + VERSION_FULL);
	DEBUG("Was built on " + PLATFORM_FULL);
	DEBUG("etcpd is licensed under the 3-clause BSD license");
	DEBUG("------------------------------------------------");
	DEBUG("1. Config parsing");
	DEBUG_NONL("Checking if " + config_file + " exists ... ");
	std::ifstream ifs(config_file);
	if(!ifs){
		DEBUG_CONT("not found");
		std::cerr << "fatal: no " + config_file + ", quitting" << std::endl;
		return 1;
	}else{
		DEBUG_CONT("found");
	}
	etcpd::parsed_data data;
	bool success = etcpd::parse(data, config_file, verbose, ifs);
	DEBUG((std::string)"Parsing " + (!success ? "failed" : "succeeded"));
	ifs.close();
	if(!success){
		std::cerr << "Config parsing failed ; sorry" << std::endl;
		return 1;
	}
	DEBUG("");
	DEBUG("2. Checking if paths in config exist");
	success = etcpd::check_config(data, verbose);
	DEBUG((std::string)"Checking " + (!success ? "failed" : "succeeded"));
	if(!success){
		std::cerr << "Checking config failed ; sorry" << std::endl;
		return 1;
	}
	DEBUG("");
	DEBUG("3. Loading core");
	etcpd::module_launcher func = etcpd::load_core(data, verbose);
	DEBUG((std::string)"Loading " + ((func == (etcpd::module_launcher)0) ? "failed" : "succeeded"));
	if(func == (etcpd::module_launcher)0){
		std::cerr << "Loading core failed ; sorry" << std::endl;
		return 1;
	}
	etcpd::module_config module_conf;
	module_conf.parsed = data;
	module_conf.verbose = verbose;
	module_conf.daemonize = daemonize;
	func(module_conf);
}
