#include <string>
#include <fstream>
#include <dlfcn.h>
#include <unistd.h>
#include <time.h>
#include <cstring>

#include "../include/util.h"
#include "../include/macro.h"

namespace etcpd {
	bool parse(etcpd::parsed_data& data, std::string path, bool verbose, std::ifstream& file){
		DEBUG("Parsing " + path);
		std::string line;
		int line_num = 0;
		while(std::getline(file, line)){
			if(line == "" || (line.size() > 0 && line[0] == '#')){
				continue;
			}
			DEBUG("");
			DEBUG_NONL("Line " + std::to_string(++line_num) + ": ");
			DEBUG_CONT(line);
			DEBUG_NONL("Command: ");
			std::string command = line.substr(0, line.find_first_of(" "));
			DEBUG_CONT(command);
			if(command == "SEARCHMOD"){
				std::string searchpath = line.substr(line.find_first_of(" ") + 1);
				DEBUG("Will search for modules on " + searchpath);
				data.searchmod.push_back(searchpath);
			}else if(command == "IMPORT"){
				std::string import_module = line.substr(line.find_first_of(" ") + 1);
				DEBUG("Will import " + import_module);
				data.import_module.push_back(import_module);
			}else if(command == "SERVER-CORE"){
				std::string corepath = line.substr(line.find_first_of(" ") + 1);
				DEBUG(corepath + " will be used for the server core");
				data.core = corepath;
			}else if(command == "PORT"){
				std::string port = line.substr(line.find_first_of(" ") + 1);
				DEBUG(port + " will be used for the port number");
				data.port = stoi(port);
			}else if(command == "MAX-CLIENTS"){
				std::string max_clients = line.substr(line.find_first_of(" ") + 1);
				DEBUG(max_clients + " will be the max number of the clients can connect");
				data.max_clients = stoi(max_clients);
#ifdef USE_SSL
			}else if(command == "USE-SSL"){
				data.ssl = line.substr(line.find_first_of(" ") + 1);
			}else if(command == "SSL-CERTIFICATE"){
				data.ssl_certificate = line.substr(line.find_first_of(" ") + 1);
			}else if(command == "SSL-KEY"){
				data.ssl_key = line.substr(line.find_first_of(" ") + 1);
#endif
			}else if(command == "SET"){
				std::string key = line.substr(line.find_first_of(" ") + 1, line.find_first_of(":") - line.find_first_of(" ") - 1);
				std::string value = line.substr(line.find_first_of(":") + 1);
				data.custom_data[key] = value;
				DEBUG(key + " = " + value);
			}else{
				DEBUG("Unknown command: " + command);
				std::cerr << "Config at line " + std::to_string(line_num) << ": Unknown command" << std::endl;
				return false;
			}
		}
		return true;
	}
	bool check_config(etcpd::parsed_data& data, bool verbose){
		for(std::string path : data.searchmod){
			DEBUG_NONL("Checking if " + path + " exists ... ");
			std::ifstream ifs(path);
			if(!ifs){
				DEBUG_CONT("not found");
				std::cerr << "Config: " << path << " was not found" << std::endl;
				return false;
			}else{
				DEBUG_CONT("found");
			}
			ifs.close();
		}

		const size_t PATH_SIZE = 128;
		char _path_cwd[PATH_SIZE];
		getcwd(_path_cwd, PATH_SIZE);
		std::string path_cwd = std::string(_path_cwd);
		for(std::string path : data.import_module){
			path = "mo_" + path;
			DEBUG_NONL("Checking if " + path + " exists ... ");
			bool found = false;
			std::string dir_module;
			for(std::string dir : data.searchmod){
				chdir(dir.c_str());
				std::ifstream ifs(path);
				if(ifs) found = true;
				ifs.close();
				dir_module = dir;
				if(found) break;
			}
			if(!found){
				DEBUG_CONT("not found");
				std::cerr << "Config: " << path << " was not found" << std::endl;
				return false;
			}else{
				DEBUG_CONT_NONL("found");
				void* lib = dlopen((dir_module + "/" + path).c_str(), RTLD_LAZY);
				if(lib == NULL){
					char* error = dlerror();
					DEBUG_CONT(", dlopen failure, error is `" + std::string(error) + "`");
					std::cerr << "Config: " << path << " caused dlopen failure. Check the error with -v" << std::endl;
					return false;
				}else{
					DEBUG_CONT(", loaded");
				}
				etcpd::server_module lib_struct;
				lib_struct.loaded = lib;
				lib_struct.init = dlsym(lib_struct.loaded, "init");
				if(lib_struct.init == NULL){
					char* error = dlerror();
					DEBUG("dlsym failure, error is `" + std::string(error) + "`");
					std::cerr << "Config: " << path << " caused dlsym failure. Check the error with -v" << std::endl;
					return false;
				}
				lib_struct.run = dlsym(lib_struct.loaded, "run");
				if(lib_struct.run == NULL){
					char* error = dlerror();
					DEBUG("dlsym failure, error is `" + std::string(error) + "`");
					std::cerr << "Config: " << path << " caused dlsym failure. Check the error with -v" << std::endl;
					return false;
				}
				lib_struct.path = dir_module + "/" + path;
				data.modules.push_back(lib_struct);
			}
		}
		chdir(_path_cwd);

		DEBUG_NONL("Checking if " + data.core + " exists ... ");
		std::ifstream ifs(data.core);
		if(!ifs){
			DEBUG_CONT("not found");
			std::cerr << "Config: " + data.core + " (aka SERVER-CORE) was not found" << std::endl;
			return false;
		}else{
			DEBUG_CONT("found");
		}
		ifs.close();
	
		if(data.ssl.size() != 0){
			DEBUG("SSL is enabled, checking key & certificate too");
			DEBUG_NONL("Checking if " + data.ssl_key + " exists ... ");
			ifs = std::ifstream(data.ssl_key);
			if(!ifs){
				DEBUG_CONT("not found");
				std::cerr << "Config: " + data.ssl_certificate + " (aka SSL-CERTIFICATE) was not found" << std::endl;
				return false;
			}else{
				DEBUG_CONT("found");
			}
			ifs.close();

			DEBUG_NONL("Checking if " + data.ssl_certificate + " exists ... ");
			ifs = std::ifstream(data.ssl_key);
			if(!ifs){
				DEBUG_CONT("not found");
				std::cerr << "Config: " + data.ssl_key + " (aka SSL-KEY) was not found" << std::endl;
				return false;
			}else{
				DEBUG_CONT("found");
			}
			ifs.close();
		}

		DEBUG_NONL("Checking if port is set ... ");
		if(data.port < 0){
			DEBUG_CONT("no");
			std::cerr << "Config: port is not set" << std::endl;
			return false;
		}else{
			DEBUG_CONT("yes");
		}

		std::string line = "--------------------------";
		DEBUG(line);
		DEBUG("These modules were loaded:");
		DEBUG("PRIORITY  | NAME");
		int priority = 1;
		for(std::string path : data.import_module){
			DEBUG_NONL(std::to_string(priority));
			for(int i = 0; i < 10 - std::to_string(priority).size(); i++){
				DEBUG_CONT_NONL(" ");
			}
			DEBUG_CONT("| " + path);
			priority++;
		}
		DEBUG(line);
		return true;
	}
	etcpd::module_launcher load_core(etcpd::parsed_data& data, bool verbose){
		etcpd::module_launcher func = (etcpd::module_launcher)0;
		void* lib = dlopen(data.core.c_str(), RTLD_LAZY);
		if(lib == NULL) return func;
		data.loaded_core = lib;
		func = (etcpd::module_launcher)dlsym(lib, "init");
		if(func == NULL){
			DEBUG("dlsym failed, error is `" + std::string(dlerror()) + "`");
			std::cerr << "Config: dlsym failed. Check the error with -v" << std::endl;
		}
		return func;
	}
	std::string date(){
		char date[128];
		time_t t = time(NULL);
		struct tm *time_st = localtime(&t);
		strftime(date, sizeof(date), "%a %b %e %H:%M:%S %Z %Y", time_st);
		return std::string(date);
	}
	std::string date(time_t t){
		char date[128];
		struct tm *time_st = localtime(&t);
		strftime(date, sizeof(date), "%a %b %e %H:%M:%S %Z %Y", time_st);
		return std::string(date);
	}
}
