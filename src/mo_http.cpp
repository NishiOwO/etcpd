#include "../include/util.h"
#include "../include/macro.h"
#include "../include/mo_http.h"

#ifdef USE_SSL
#include <openssl/ssl.h>
#endif

#include <dlfcn.h>
#include <unistd.h>
#include <cstring>
#include <map>
#include <sstream>
#include <algorithm>
#include <vector>

std::vector<void*> loaded_modules;
std::vector<std::string> loaded_modules_path;

void mo_http::http_response::http_header(){
	gload(this->config);
	gwrite(this->config, "HTTP/", 5);
	gwrite(this->config, this->version.c_str(), this->version.size());
	gwrite(this->config, " ", 1);
	gwrite(this->config, std::to_string(this->status_code).c_str(), std::to_string(this->status_code).size());
	gwrite(this->config, " ", 1);
	gwrite(this->config, this->status_text.c_str(), this->status_text.size());
	gwrite(this->config, "\r\n", 2);

	for(std::string header : this->headers){
		gwrite(this->config, header.c_str(), header.size());
		gwrite(this->config, "\r\n", 2);
	}

	gwrite(this->config, "\r\n", 2);
}

void mo_http::http_response::write_all(){
	gload(this->config);
	this->http_header();
	gwrite(this->config, this->body.c_str(), this->body.size());
}

extern "C" void init(etcpd::module_config config){
	bool verbose = config.verbose;
	DEBUG("mo_http.so v1.0.0");
	DEBUG("Phase 1: Parsing and loading HTTP-MODULES");
	if(config.parsed.custom_data.find("HTTP-MODULES") != config.parsed.custom_data.end()){
		int counter = 0;
		int success_counter = 0;
		std::string path = "";
		auto load_mod = [&]{
			DEBUG_NONL(path);
			DEBUG_CONT_NONL(" (");
			void* lib_mod = dlopen(path.c_str(), RTLD_LAZY);
			if(lib_mod == NULL){
				DEBUG_CONT_NONL("failed, `" + std::string(dlerror()) + "`");
			}else{
				DEBUG_CONT_NONL("success");
				success_counter++;
				loaded_modules_path.push_back(path);
				loaded_modules.push_back(lib_mod);
			}
			DEBUG_CONT(")");
			path = "";
			counter++;
		};
		for(char c : config.parsed.custom_data["HTTP-MODULES"]){
			if(c == ','){
				load_mod();
			}else{
				path += c;
			}
		}
		if(path.size() != 0){
			load_mod();
		}
		DEBUG(std::to_string(counter) + " module(s) have been tried to be loaded");
		DEBUG(std::to_string(success_counter) + " module(s) have been loaded successfully");

		counter = 0;
		success_counter = 0;
		DEBUG("Phase 2: Initialising HTTP-MODULES");
		for(int i = 0; i < loaded_modules.size(); i++){
			DEBUG_NONL(loaded_modules_path[i]);
			void* func = dlsym(loaded_modules[i], "init");
			DEBUG_CONT_NONL(" (");
			if(func == NULL){
				DEBUG_CONT_NONL("failed, `" + std::string(dlerror()) + "`");
				DEBUG_CONT(")");
			}else{
				DEBUG_CONT_NONL("success");
				DEBUG_CONT(")");
				etcpd::module_config conf;
				//conf.verbose = config.verbose;
				//conf.parsed = config.parsed;
				conf = config;
				((etcpd::module_launcher)func)(conf);
				success_counter++;
			}
			path = "";
			counter++;
		}
		DEBUG(std::to_string(counter) + " module(s) have been tried to be loaded");
		DEBUG(std::to_string(success_counter) + " module(s) have been loaded successfully");
	}else{
		DEBUG("HTTP-MODULES were not set");
		DEBUG("mo_http.so is useless without HTTP modules, though...");
	}
}

extern "C" int run(etcpd::module_config config){
	bool verbose = config.verbose;
	DEBUG("[" + std::to_string(config.client_socket) + "] run@mo_http.so");

	gload(config);

	int phase = 0;

	int bytes_per_read = 512;
	if(config.parsed.custom_data.find("HTTP-BYTES-PER-READ") != config.parsed.custom_data.end()){
		bytes_per_read = stoi(config.parsed.custom_data["HTTP-BYTES-PER-READ"]);
	}
	char* buf = (char*)malloc(bytes_per_read);
	std::string buf_string = "";
	int counter = 0;

	mo_http::http_request request;

	while(true){
repeat:
		if(counter == 0){
			int code = gread(config, buf, bytes_per_read);
			if(code <= 0){
				break;
			}
		}
		buf_string += buf[counter];
		counter++;
		if(counter == bytes_per_read){
			counter = 0;
		}
		if(phase == 0 && !(buf_string.size() >= 2 && buf_string.substr(buf_string.size() - 2) == "\r\n")) goto repeat;
		if(phase == 1 && !(buf_string.size() >= 4 && buf_string.substr(buf_string.size() - 4) == "\r\n\r\n")) goto repeat;
		if(phase == 2 && !(buf_string.size() >= stoi(request.headers["content-length"]))) goto repeat;
		try{
			if(phase == 0){
				std::string first_line = buf_string.substr(0, buf_string.find_first_of("\r\n"));
				request.method = first_line.substr(0, first_line.find_first_of(" "));
				request.full_path = first_line.substr(request.method.size() + 1, first_line.find_first_of(" ", request.method.size()+1) - request.method.size() - 1);
				request.raw_path = request.full_path;
				std::string buffer = "";
				std::string buffer_path = "";
				bool url_encoding = false;
				int counter = 0;
				for(int i = 0; i < request.full_path.size(); i++){
					if(request.full_path[i] == '%'){
						if(url_encoding){
							buffer_path += "%";
							buffer = "";
						}else{
							url_encoding = true;
						}
					}else if(url_encoding){
						buffer += request.full_path[i];
						counter++;
						if(counter == 2){
							char result = 0;
							for(int j = 0; j < counter; j++){
								result <<= 4;
								if(buffer[j] >= 'a' && buffer[j] <= 'f'){
									result += buffer[j] - 'a' + 10;
								}else if(buffer[j] >= 'A' && buffer[j] <= 'F'){
									result += buffer[j] - 'A' + 10;
								}else if(buffer[j] >= '0' && buffer[j] <= '9'){
									result += buffer[j] - '0';
								}
							}
							buffer_path += result;
							counter = 0;
							buffer = "";
							url_encoding = false;
						}
					}else{
						buffer_path += request.full_path[i];
					}
				}
				request.full_path = buffer_path;
				request.path = request.full_path.substr(0, request.full_path.find_first_of('?'));
				request._path = request.path;
				int i;
				for(i = request.path.size() - 1; i != 0 && request.path[i] == '/'; i--);
				request.path = request.path.substr(0, i + 1);
				request.raw_version = first_line.substr(request.method.size() + request.raw_path.size() + 2);
				request.version = request.raw_version.substr(request.raw_version.find_first_of("/") + 1);
				DEBUG("[" + std::to_string(config.client_socket) + "] Method is " + request.method);
				DEBUG("[" + std::to_string(config.client_socket) + "] Path is `" + request.path + "`");
				DEBUG("[" + std::to_string(config.client_socket) + "] Version is " + request.version);
				phase++;
				buf_string = "";
			}else if(phase == 1){
				std::stringstream ss{buf_string.substr(0, buf_string.size() - 4)};
				std::string line;
				while(std::getline(ss, line)){
					std::string formatted = "";
					for(char c : line){
						if(c != '\r' && c != '\n') formatted += c;
					}
					std::string header_key = formatted.substr(0, formatted.find_first_of(":"));
					std::string _header_value = formatted.substr(formatted.find_first_of(":") + 1);
					std::string header_value = "";
					bool space = true;
					for(char c : _header_value){
						if(!(c == ' ' && space)){
							header_value += c;
						}else if(c != ' ' && space){
							space = false;
						}
					}
					std::transform(header_key.begin(), header_key.end(), header_key.begin(), [](unsigned char c){return std::tolower(c);});
					request.headers[header_key] = header_value;
				}
				DEBUG("[" + std::to_string(config.client_socket) + "] Header parsing done");
				if(request.method == "POST"){
					DEBUG("[" + std::to_string(config.client_socket) + "] Got " + request.method + " method, will parse the body too");
					phase++;
					buf_string = "";
				}else{
					request.body = "";
					break;
				}
			}else if(phase == 2){
				request.body = buf_string;
				break;
			}
		}catch(...){
			DEBUG("[" + std::to_string(config.client_socket) + "] Invalid HTTP");
			std::cerr << strerror(errno) << std::endl;
			if(config.parsed.custom_data.find("CLOSE-ON-INVALID") != config.parsed.custom_data.end()){
				DEBUG("[" + std::to_string(config.client_socket) + "] CLOSE-ON-INVALID is set");
				gclose(config);
				return etcpd::MODULE_CLOSE | etcpd::MODULE_BREAK;
			}
		}
	}

	free(buf);

	if(request._path[request._path.size() - 1] != '/'){
		mo_http::http_response response;
		response.config = config;
		response.headers.push_back("Location: " + request._path + "/");
		response.status_code = 301;
		response.status_text = "Moved Permanentaly";
		response.http_header();
		return etcpd::MODULE_CLOSE | etcpd::MODULE_BREAK;
	}

	mo_http::http_response response;

	for(int i = 0; i < loaded_modules.size(); i++){
		void* func = dlsym(loaded_modules[i], "run");
		etcpd::module_config conf;
		conf = config;
		response.config = conf;
		request.config = conf;
		int result = ((mo_http::launcher)func)(conf, request, response);
		parse_result(result, config, loaded_modules_path[i]);
	}

	return etcpd::MODULE_CLOSE | etcpd::MODULE_BREAK;
}
