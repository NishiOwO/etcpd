# etcpd - 拡張可能TCP/IPデーモン
etcpdはC++で書かれたTCP/IPのデーモンです。

## サポートされているOS
- [x] NetBSD
- [x] 他のBSD (テストはしていませんが、動くはずです。)
- [x] OSX (これもテストはしていませんが、動くはずです。)
- [x] Linux (Void GNU/Linuxのカーネル6.xで動作確認済みです。)
- [ ] Windows (将来的にはサポートするかもしれませんが、Windows機を持っていないのでテストのしようがありません...)

## 機能
- 「モジュール」機能
- 速い
- モジュールも含めて1MB以下(strip後)。

## デフォルトのモジュール
- HTTP/1.1モジュール
  - ファイルの取得
  - インデックス

## etcpdをビルドする
### 必要なライブラリやツール
- OpenSSL (`--enable-ssl`が`./configure`に渡された時のみ必要です。)
- GNU make (pkgsrcには`gmake`というのがGNU makeです。)

`--enable-pthread`を`./configure`に渡せばpthreadをマルチスレッド用に使います。(デフォルトではforkを使用します。)

```shell-session
$ ./configure --prefix=/usr
$ gmake all
# gmake install
```
