#ifndef __ETCPD_MO_HTTP_H__
#define __ETCPD_MO_HTTP_H__

#include "../include/util.h"
#include <string>
#include <map>

#define GET_MIME(x) { \
		std::string ext = x.substr(1 + x.find_last_of(".")); \
		if(config.parsed.custom_data.find("HTTP-MIME-" + ext) == config.parsed.custom_data.end()){ \
			if(config.parsed.custom_data.find("HTTP-MIME-DEFAULT") == config.parsed.custom_data.end()){ \
				mime = "text/plain"; \
			}else{ \
				mime = config.parsed.custom_data["HTTP-MIME-DEFAULT"]; \
			} \
		}else{ \
			mime = config.parsed.custom_data["HTTP-MIME-" + ext]; \
		} \
	}

#define HTTP_FILE_CHECK(x) { \
		if(config.parsed.custom_data.find("HTTP-" + std::to_string(x)) == config.parsed.custom_data.end()){ \
			std::string error = _config_error(config, "mo_http_basic.so: HTTP-" + std::to_string(x) + " is not set"); \
			response.status_code = 500; \
			response.status_text = "Internal Server Error"; \
			response.headers.push_back("Content-Length: " + std::to_string(error.size())); \
			response.headers.push_back("Content-Type: text/html"); \
			response.http_header(); \
			gwrite(config, error.c_str(), error.size()); \
			return etcpd::MODULE_BREAK; \
		} \
		std::string path_http_err; \
		path_http_err = "HTTP-" + std::to_string(x) + (config.parsed.custom_data["HTTP-" + std::to_string(x)] == "ROOT" ? "-ROOT" : ""); \
		if(config.parsed.custom_data["HTTP-" + std::to_string(x)] == "ROOT"){ \
			path_http_result = config.parsed.custom_data["HTTP-ROOT"] + "/" + config.parsed.custom_data[path_http_err]; \
		}else{ \
			path_http_result = config.parsed.custom_data[path_http_err]; \
		} \
		ifs = std::ifstream(path_http_result); \
		if(!ifs){ \
			std::string error = _config_error(config, "mo_http_basic.so: File specified by " + path_http_err + " is non existent"); \
			response.status_code = 500; \
	 		response.status_text = "Internal Server Error"; \
			response.headers.push_back("Content-Length: " + std::to_string(error.size())); \
			response.headers.push_back("Content-Type: text/html"); \
			response.http_header(); \
			gwrite(config, error.c_str(), error.size()); \
			return etcpd::MODULE_BREAK; \
		} \
		stat(path_http_result.c_str(), &s);\
	}

namespace mo_http {
	const unsigned char SUCCESS	= 0b00000001;
	const unsigned char ERROR	= 0b10000000;

	struct http_request {
		std::string method;
		std::string raw_version;
		std::string version;
		std::string raw_path;
		std::string path;
		std::string _path;
		std::string full_path;
		std::string body;
		std::map<std::string, std::string> headers;
		unsigned char code = SUCCESS;
		etcpd::module_config config;
	};

	struct http_response {
		std::string version = "1.1";
		int status_code = 200;
		std::string status_text = "OK";
		std::vector<std::string> headers;
		etcpd::module_config config;
		std::string body = "";
		void http_header();
		void write_all();
	};

	typedef int(*launcher)(etcpd::module_config, mo_http::http_request, mo_http::http_response);
}

#endif
