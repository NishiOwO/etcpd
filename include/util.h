#ifndef __UTIL_H__
#define __UTIL_H__

#include <fstream>
#include <string>
#include <vector>

#ifdef USE_SSL
#include <openssl/ssl.h>
#endif

#include <map>
#include <unistd.h>
#include <sys/socket.h>
#include <dlfcn.h>
#include <time.h>
#include <cstring>

#include "../include/macro.h"

#define gload(config) etcpd::gwrite_t gwrite = (etcpd::gwrite_t)dlsym(config.parsed.loaded_core, "gwrite"); \
	etcpd::gread_t gread = (etcpd::gread_t)dlsym(config.parsed.loaded_core, "gread"); \
	etcpd::gclose_t gclose = (etcpd::gclose_t)dlsym(config.parsed.loaded_core, "gclose"); \
	etcpd::gversion_t gversion = (etcpd::gversion_t)dlsym(config.parsed.loaded_core, "gversion"); \
	etcpd::gplatform_t gplatform = (etcpd::gplatform_t)dlsym(config.parsed.loaded_core, "gplatform"); \
	etcpd::grunning_t grunning = (etcpd::grunning_t)dlsym(config.parsed.loaded_core, "grunning"); \

#ifdef USE_SSL
#define parse_result(result, config, path) {\
		int result_flag = result & 0b11111111;\
		if(result_flag & etcpd::MODULE_CLOSE){\
			DEBUG(path + " emitted MODULE_CLOSE");\
			if(config.parsed.ssl.size() != 0){\
				SSL_shutdown(config.ssl);\
				SSL_free(config.ssl);\
			}\
			shutdown(config.client_socket, SHUT_RDWR);\
			close(config.client_socket);\
		}\
		if(result_flag & etcpd::MODULE_BREAK){\
			DEBUG(path + " emitted MODULE_BREAK");\
			break;\
		}\
		if(result_flag & etcpd::MODULE_CONTINUE){\
			DEBUG(path + " emitted MODULE_CONTINUE");\
			continue;\
		}\
		if(result_flag & etcpd::MODULE_EXIT){\
			DEBUG(path + " emitted MODULE_EXIT with exit code " + std::to_string(result >> 8));\
			exit(result >> 8);\
		}\
	}	
#else
#define parse_result(result, config, path) {\
		int result_flag = result & 0b11111111;\
		if(result_flag & etcpd::MODULE_CLOSE){\
			DEBUG(path + " emitted MODULE_CLOSE");\
			shutdown(config.client_socket, SHUT_RDWR);\
			close(config.client_socket);\
		}\
		if(result_flag & etcpd::MODULE_BREAK){\
			DEBUG(path + " emitted MODULE_BREAK");\
			break;\
		}\
		if(result_flag & etcpd::MODULE_CONTINUE){\
			DEBUG(path + " emitted MODULE_CONTINUE");\
			continue;\
		}\
		if(result_flag & etcpd::MODULE_EXIT){\
			DEBUG(path + " emitted MODULE_EXIT with exit code " + std::to_string(result >> 8));\
			exit(result >> 8);\
		}\
	}	
#endif

namespace etcpd {
	const int MODULE_BREAK		= 0b00000001;
	const int MODULE_CONTINUE	= 0b00000010;
	const int MODULE_EXIT		= 0b00000100;
	const int MODULE_CLOSE		= 0b10000000;

	struct server_module {
		void* loaded;
		void* init;
		void* run;
		std::string path;
	};
	
	struct parsed_data {
		std::vector<etcpd::server_module> modules;
		std::vector<std::string> import_module;
		std::vector<std::string> searchmod;
		std::string core;
		void* loaded_core;
		int port = -1;
		std::string ssl = "";
		std::string ssl_certificate = "";
		std::string ssl_key = "";
		std::map<std::string, std::string> custom_data;
		int max_clients = 5;
	};

	struct module_config {
		std::string ip;
		bool verbose;
#ifdef USE_SSL
		SSL* ssl = NULL;
#endif
		int client_socket;
		etcpd::parsed_data parsed;
		std::map<std::string, void*> custom_void;
		bool daemonize;
	};

	typedef int(*module_launcher)(etcpd::module_config config);

	typedef ssize_t(*gwrite_t)(etcpd::module_config config, const void* buf, size_t size);
	typedef ssize_t(*gread_t)(etcpd::module_config config, void* buf, size_t size);
	typedef int(*gclose_t)(etcpd::module_config config);
	typedef std::string(*gversion_t)();
	typedef gversion_t gplatform_t;
	typedef gversion_t grunning_t;

	template <typename K, typename V>
	bool find_value(std::map<K, V> map_data, K key){
		return map_data.find(key) != map_data.end();
	}
	
	bool parse(etcpd::parsed_data& data, std::string path, bool verbose, std::ifstream& file);
	bool check_config(etcpd::parsed_data& data, bool verbose);
	etcpd::module_launcher load_core(etcpd::parsed_data& data, bool verbose);
	std::string date();
	std::string date(time_t);
}

#endif
