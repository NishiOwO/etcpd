#ifndef __ETCPD_MACRO_H__
#define __ETCPD_MACRO_H__

#include <iostream>
#define DEBUG(x) if(verbose) std::cout << __FILE__ << ": Line " << __LINE__ << ": " << (x) << std::endl
#define DEBUG_NONL(x) if(verbose) std::cout << __FILE__ << ": Line " << __LINE__ << ": " << (x)
#define DEBUG_CONT(x) if(verbose) std::cout << (x) << std::endl
#define DEBUG_CONT_NONL(x) if(verbose) std::cout << (x)

#endif
