#ifndef __INFO_H__
#define __INFO_H__

#include <string>
#define VERSION_PREFIX "r"
#define VERSION_MAJOR 1
#define VERSION_MINOR 6
#define VERSION_PATCH 2

#ifdef USE_PTHREAD
#define MULTITHREAD "pthread"
#else
#define MULTITHREAD "fork"
#endif

#ifdef USE_SSL
#define SECURITY "ssl"
#else
#define SECURITY "nossl"
#endif

#define VERSION_FULL std::string(VERSION_PREFIX) + std::to_string(VERSION_MAJOR) + "." + std::to_string(VERSION_MINOR) + "." + std::to_string(VERSION_PATCH) + "-" + std::string(MULTITHREAD) + "-" + std::string(SECURITY)

#define PLATFORM_OS std::string(_PLATFORM_OS)
#define PLATFORM_RELEASE std::string(_PLATFORM_RELEASE)
#define PLATFORM_MACHINE std::string(_PLATFORM_MACHINE)
#define PLATFORM_FULL PLATFORM_OS + "/" + PLATFORM_MACHINE + " " + PLATFORM_RELEASE

#endif
